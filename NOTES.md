Shorty Challenge - Implementation Notes
=======================================

## On the tools used

I tried to use as few tools as possible to meet the requirements of the challenge. <i>Sinatra</i> seemed to be the consensual framework for building simple API such as this one, as well as the use of <i>RSpec</i> and <i>Rack-Test</i> for testing.

As for the data store used, <i>Redis</i> seemed to be a good fit for the problem, easy to use for this quick development challenge but robust for a real world application (see the next section for more).
<i>Ohm</i>, together with <i>Redic</i> facilitated this integration.

As for running the API and the test suite, I won't assume you have anything in whatever machine you choose to run them beside Docker and Docker Compose. Hence, to run only two commands are needed:

+ To run the API and expose the service on port <i>4567</i>:
<b>docker-compose up</b>
+ To run the test suite:
<b>docker-compose -f docker-compose.test.yml run --rm shorty-test</b>

## On the data store

Redis was used as a data store. Given the requirements of the problem, Redis' key-value storage was chosen over a relational database, since there seems to be no need for relational information.

These systems should have many more READ than WRITE operations. Redis data can be partitioned and replicated to slave nodes, ensuring that the systems can scale both in storage and in number of requests served, while the use of Redis Sentinel will provide failover protection in case the master node goes down.


## On the shortcode generation

Concerning the generation of shortcodes, there are three main requirements:
1. The system must be able to generate random 6 character shortcodes ;
2. It must be possible for the user to request a specific shortcode, even a 6 character one;
3. All codes must be composed of characters in the following set <b>"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_"</b>;

Of the multiple strategies that could be used to tackle the first requirement, I opted for generating random numbers and converting them to the Base63 from point 3. This ensures that all generated codes are random and unordered.

However, we have a limit of 63<sup>6</sup> = 62523502209 codes that can be generated. Since these are being randomly generated, there always exists a probability that a new code has already been previously generated and stored. According to the <i><a href="https://en.wikipedia.org/wiki/Birthday_problem">Birthday Problem</a></i>, after only 294408 generated codes, the probability of collision will be close to 50% and increasing rapidly, requiring running the generator multiple times and checking the data store for the existence of a code for each of them.

An alternative to this would be to use a tool like <i><a href="https://hashids.org/">Hashids</a></i>, which converts numbers to a given base in a unique and non-sequential way. By knowing just how many codes had been previously generated, the next seemingly random code could be found, without the possibility of collisions occurring. However requirement 2 states that the client can request a specific (6 character) code, rendering this sequential method unusable.

Increasing the number of characters for the auto-generated codes would be a possible improvement to the system. As an example, for 8 character codes, 50% chance of collision would only happen after ~18 Million codes, and for 10 character codes, only after 1170 Million.


## On the url validation

The requirements state that a URL can be shortened, but don't specify which types of urls are accepted. I took the liberty of validating urls allowing only those that could be relative to web pages (http and https).