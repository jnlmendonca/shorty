require 'redic'
require 'ohm'
require 'ohm/contrib'

Ohm.redis = Redic.new(ENV.fetch('REDIS_URL'))


class ShortUrl < Ohm::Model
    include Ohm::Timestamps

    attribute :shortcode
    attribute :url
    counter :redirect_count

    index :shortcode
    unique :shortcode

    def stats_as_json()
        data = {
            startDate: self.created_at.iso8601,
            redirectCount: self.redirect_count
        }
        data[:lastSeenDate] = self.updated_at.iso8601 if self.redirect_count > 0

        return data.to_json
    end

    def shortcode_as_json
        return {
            shortcode: self.shortcode
        }.to_json
    end
end


def create_shortcode(shortcode, url)
    return ShortUrl.create(shortcode: shortcode, url: url).shortcode_as_json
end


def exists_shortcode(shortcode)
    return ShortUrl.find(shortcode: shortcode).size > 0
end


def access_shortcode(shortcode)
    short_url = ShortUrl.find(shortcode: shortcode).first
    if short_url
        short_url.increment :redirect_count
        short_url.save
        return short_url.url
    else
        return nil
    end
end


def stats_shortcode(shortcode)
    short_url = ShortUrl.find(shortcode: shortcode).first
    if short_url
        return short_url.stats_as_json
    else
        return nil
    end
end


def delete_all_shortcodes
    ShortUrl.all.each {|short_url| short_url.delete}
end