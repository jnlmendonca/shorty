require 'sinatra/base'
require_relative 'codes'
require_relative 'db'
require_relative 'url'


class ShortyApp < Sinatra::Base

    set :bind, '0.0.0.0'

    # Parse request body as JSON for '/shorten' route
    before do
    end

    # Endpoint to generate new shortcodes
    post '/shorten' do

        request.body.rewind
        request_payload = request.body.read.to_s

        # Test for empty request body
        halt 400, {'Content-Type' => 'application/json'}, { description: 'request has no payload' }.to_json if request_payload.empty?

        # Test for content-type
        halt 415, {'Content-Type' => 'application/json'}, { description: 'request must have \'application/json\' Content-Type' }.to_json unless request.media_type.eql? "application/json"

        # Test url presence in request body
        request_payload_json = JSON.parse(request_payload)
        halt 400, {'Content-Type' => 'application/json'}, { description: 'url is not present' }.to_json unless request_payload_json.key? "url"

        # Test url validity
        halt 400, {'Content-Type' => 'application/json'}, { description: 'url is not valid' }.to_json unless is_url(request_payload_json['url'])

        # Test shortcode presence in request body
        if request_payload_json.key? "shortcode"
            shortcode = request_payload_json["shortcode"]
            halt 422, {'Content-Type' => 'application/json'}, { description: 'The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{4,}$' }.to_json unless is_shortcode_format_valid(shortcode)
            halt 409, {'Content-Type' => 'application/json'}, { description: 'The the desired shortcode is already in use. Shortcodes are case-sensitive.' }.to_json if exists_shortcode(shortcode)
            short_url_json = create_shortcode(shortcode, request_payload_json['url'])
            return 201, {'Content-Type' => 'application/json'}, short_url_json
        end

        # Create shortcode
        while true
            shortcode = get_random_shortcode()
            if !exists_shortcode(shortcode)
                short_url_json = create_shortcode(shortcode, request_payload_json['url'])
                return 201, {'Content-Type' => 'application/json'}, short_url_json
            end
        end

    end

    # Endpoint to redirect to original url
    get '/:shortcode' do
        shortcode_url = access_shortcode(params[:shortcode])
        halt 404, {'Content-Type' => 'application/json'}, { description: 'The shortcode cannot be found in the system' }.to_json if shortcode_url.nil?
        halt 302, {'Location' => shortcode_url}, ""
    end

    # Endpoint to get shortened url stats
    get '/:shortcode/stats' do
        shortcode_stats_json = stats_shortcode(params[:shortcode])
        halt 404, {'Content-Type' => 'application/json'}, { description: 'The shortcode cannot be found in the system' }.to_json if shortcode_stats_json.nil?
        halt 200, {'Content-Type' => 'application/json'}, shortcode_stats_json
    end
end
