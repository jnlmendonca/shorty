require 'set'

BASE_CHARACTERS = (0..9).to_a.map(&:to_s) + ('a'..'z').to_a + ('A'..'Z').to_a + ['_']
BASE_CHARACTER_SET = Set.new(BASE_CHARACTERS)
BASE_DIMENSION = BASE_CHARACTERS.length
MIN_SHORTCODE_LENGTH = 4
SHORTCODE_VALIDATION_REGEXP = /^[0-9a-zA-Z_]{4,}$/


def get_random_shortcode(length = 6)

    if length < MIN_SHORTCODE_LENGTH
        return nil
    end

    max_allowed_numbers = BASE_DIMENSION ** length

    # Generate a random number and convert to shortcode
    shortcode = convert_to_shortcode(rand(max_allowed_numbers))
    shortcode.rjust(length, '0')

    return shortcode
end


def convert_to_shortcode(number)

    if number == 0
        return '0'
    end

    dividend = number
    shortcode = ''
    while dividend > 0 do
        remainder = dividend % BASE_DIMENSION
        shortcode_character = BASE_CHARACTERS[remainder]
        shortcode = shortcode_character + shortcode
        dividend = dividend / BASE_DIMENSION
    end

    return shortcode
end


def convert_to_number(shortcode)

    if shortcode.empty?
        return nil
    end

    number = 0
    shortcode_characters = shortcode.split("")
    shortcode_characters.each_with_index do |character, index|
        number += (BASE_DIMENSION ** (shortcode.length - 1 - index)) * BASE_CHARACTERS.index(character)
    end

    return number
end


def is_shortcode_format_valid(shortcode)

    if SHORTCODE_VALIDATION_REGEXP.match?(shortcode)
        return true
    else
        return false
    end

end
