require 'codes.rb'

describe '#is_shortcode_format_valid' do
	let(:valid_shortcode) {'ABcd12_'}
	let(:invalid_shortcode) {'qw34AS_?'}

	context 'when called with valid shortcode' do
		subject(:shortcode_valid_response) { is_shortcode_format_valid(valid_shortcode) }

		it 'should return true' do
		 	expect(shortcode_valid_response).to eq(true)
		end
	end

	context 'when called with invalid shortcode' do
		subject(:shortcode_invalid_response) { is_shortcode_format_valid(invalid_shortcode) }

		it 'should return false' do
		 	expect(shortcode_invalid_response).to eq(false)
		end
	end
end

describe '#get_random_shortcode' do

	let(:random_valid_shortcode_length) {rand(4..1000)}
	let(:random_invalid_shortcode_length) {rand(0..3)}

	context 'when called with no arguments' do
		subject(:random_shortcode) { get_random_shortcode }

		it 'should return a 6 character valid code' do
			expect(random_shortcode.length).to eq(6)
			expect(is_shortcode_format_valid(random_shortcode)).to eq(true)
		end
	end

	context 'when called with an integer greater than 3' do
		subject(:fixed_length_shortcode) { get_random_shortcode(random_valid_shortcode_length) }

		it 'should return a 6 character valid code' do
			expect(fixed_length_shortcode.length).to eq(random_valid_shortcode_length)
			expect(is_shortcode_format_valid(fixed_length_shortcode)).to eq(true)
		end
	end

	context 'when called with an integer lower than 4' do
		subject(:fixed_length_shortcode) { get_random_shortcode(random_invalid_shortcode_length) }

		it 'should return a 6 character valid code' do
			expect(fixed_length_shortcode).to eq(nil)
		end
	end
end

describe '#convert_to_shortcode' do

	it 'should convert 0 to shortcode "0"' do
		expect(convert_to_shortcode(0)).to eq('0')
	end

	it 'should convert 10 to shortcode "a"' do
		expect(convert_to_shortcode(10)).to eq('a')
	end
	
	it 'should convert 62 to shortcode "_"' do
		expect(convert_to_shortcode(62)).to eq('_')
	end
	
	it 'should convert (63 ** 6 - 1) to shortcode "______"' do
		expect(convert_to_shortcode(63 ** 6 - 1)).to eq('______')
	end
end

describe '#convert_to_number' do

	it 'should convert shortcode "0" to 0' do
		expect(convert_to_number('0')).to eq(0)
	end

	it 'should convert shortcode "a" to 10' do
		expect(convert_to_number('a')).to eq(10)
	end
	
	it 'should convert shortcode "_" to 62' do
		expect(convert_to_number('_')).to eq(62)
	end
	
	it 'should convert shortcode "______" to (63 ** 6 - 1)' do
		expect(convert_to_number('______')).to eq(63 ** 6 - 1)
	end
end
