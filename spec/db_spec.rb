require 'db.rb'


describe '#delete_all_shortcodes' do
	
	context 'when called' do

		before {delete_all_shortcodes}

		it 'makes all records in database disappear' do
			expect(ShortUrl.all.size).to eq(0)
		end
	end
end


describe '#create_shortcode' do

	let(:shortcode) {'AAAAAA'}
  	let(:url) {'http://url1.com'}
	
	context 'when called first time whith shortcode' do
		subject(:shortcode_creation_response) { create_shortcode(shortcode, url) }

		it 'returns JSON with "shortcode"' do
			expect(!!JSON.parse(shortcode_creation_response)).to eq(true)
			expect(shortcode_creation_response).to eq({shortcode: shortcode}.to_json)
		end
	end

	context 'when called second time with shortcode' do
		subject(:shortcode_creation_response) { create_shortcode(shortcode, url) }

		it 'raises an error' do
			expect{shortcode_creation_response}.to raise_error Ohm::UniqueIndexViolation
		end
	end
end


describe '#exists_shortcode' do

	let(:known_shortcode) {'AAAAAA'}
	let(:unknown_shortcode) {'BBBBBB'}
	
	context 'when called with known shortcode' do
		subject(:shortcode_existence_response) { exists_shortcode(known_shortcode) }

		it 'returns "true"' do
			expect(shortcode_existence_response).to eq(true)
		end
	end
	
	context 'when called with unknown shortcode' do
		subject(:shortcode_existence_response) { exists_shortcode(unknown_shortcode) }

		it 'returns "false"' do
			expect(shortcode_existence_response).to eq(false)
		end
	end
end


describe '#access_shortcode' do

	let(:known_shortcode) {'AAAAAA'}
  	let(:known_shortcode_url) {'http://url1.com'}
	let(:unknown_shortcode) {'BBBBBB'}
	
	context 'when called with known shortcode' do
		subject(:shortcode_access_response) { access_shortcode(known_shortcode) }

		it 'returns "url"' do
			expect(shortcode_access_response).to eq(known_shortcode_url)
		end
	end
	
	context 'when called with unknown shortcode' do
		subject(:shortcode_access_response) { access_shortcode(unknown_shortcode) }

		it 'returns "nil"' do
			expect(shortcode_access_response).to eq(nil)
		end
	end
end


describe '#stats_shortcode' do

	let(:known_shortcode) {'AAAAAA'}
  	let(:known_shortcode_url) {'http://url1.com'}
	let(:unknown_shortcode) {'BBBBBB'}
	let(:new_shortcode) {'CCCCCC'}
  	let(:new_shortcode_url) {'http://url2.com'}
	
	context 'when called with newly created shortcode' do
		before { create_shortcode(new_shortcode, new_shortcode_url) }
		subject(:shortcode_stats_response) { stats_shortcode(new_shortcode) }

		it 'returns JSON with "redirectCount" (zero) and "startDate"' do
			expect(!!JSON.parse(shortcode_stats_response)).to eq(true)
			expect(JSON.parse(shortcode_stats_response)).to have_key('redirectCount')
			expect(JSON.parse(shortcode_stats_response)['redirectCount']).to eq(0)
			expect(JSON.parse(shortcode_stats_response)).to have_key('startDate')
			expect(JSON.parse(shortcode_stats_response)).not_to have_key('lastSeenDate')
		end
	end
	
	context 'when called with a shortcode previously accessed' do
		subject(:shortcode_stats_response) { stats_shortcode(known_shortcode) }

		it 'returns JSON with "redirectCount" (one), "startDate" and, "lastSeenDate"' do
			expect(!!JSON.parse(shortcode_stats_response)).to eq(true)
			expect(JSON.parse(shortcode_stats_response)).to have_key('redirectCount')
			expect(JSON.parse(shortcode_stats_response)['redirectCount']).to eq(1)
			expect(JSON.parse(shortcode_stats_response)).to have_key('startDate')
			expect(JSON.parse(shortcode_stats_response)).to have_key('lastSeenDate')
		end
	end
	
	context 'when called with unknown shortcode' do
		subject(:shortcode_stats_response) { stats_shortcode(unknown_shortcode) }

		it 'returns nil' do
			expect(shortcode_stats_response).to eq(nil)
		end
	end
end
