require 'shorty.rb'
require 'codes.rb'
require 'rack/test'

describe 'Shorty App' do
	include Rack::Test::Methods

	def app
		ShortyApp.new
	end

	let(:valid_shortcode) {'ABqw12_'}
	let(:invalid_shortcode) {'ABqw12_?'}
	let(:unknown_shortcode) {'ZZZZZZ'}
	let(:example_url_1) {'http://example1.shorty.impraise.com'}
	let(:known_shortcode) {'AAAAAA'}
	let(:new_shortcode) {'CCCCCC'}

	context 'when receiving a POST request to "/shorten" with no payload' do
		it 'returns [400]' do
			post '/shorten'
			expect(last_response.status).to eq(400)
			expect(last_response.headers['Content-Type']).to eq('application/json')
		end
	end

	context 'when receiving a POST request to "/shorten" with empty payload and "text/plain" Content-Type' do
		it 'returns [415]' do
			post '/shorten', {}.to_json, { "CONTENT_TYPE" => "text/plain" }
			expect(last_response.status).to eq(415)
			expect(last_response.headers['Content-Type']).to eq('application/json')
		end
	end

	context 'when receiving a POST request to "/shorten" with empty payload and "application/json" Content-Type' do
		it 'returns [400]' do
			post '/shorten', {}.to_json, { "CONTENT_TYPE" => "application/json" }
			expect(last_response.status).to eq(400)
			expect(last_response.headers['Content-Type']).to eq('application/json')
		end
	end

	context 'when receiving a POST request to "/shorten" with payload not containing "url"' do
		it 'returns [400]' do
			post '/shorten', { not_url: example_url_1 }.to_json, { "CONTENT_TYPE" => "application/json" }
			expect(last_response.status).to eq(400)
			expect(last_response.headers['Content-Type']).to eq('application/json')
		end
	end

	context 'when receiving a POST request to "/shorten" with payload containing "url"' do

		it 'returns [201] with a valid "shortcode"' do
			post '/shorten', { url: example_url_1 }.to_json, { "CONTENT_TYPE" => "application/json" }
			expect(last_response.status).to eq(201)
			expect(is_shortcode_format_valid(JSON.parse(last_response.body)["shortcode"])).to eq(true)
			expect(last_response.headers['Content-Type']).to eq('application/json')
		end
	end

	context 'when receiving a POST request to "/shorten" with payload containing "url" and valid "shortcode"' do

		it 'returns [201] with the valid "shortcode"' do
			post '/shorten', { url: example_url_1, shortcode: valid_shortcode }.to_json, { "CONTENT_TYPE" => "application/json" }
			expect(last_response.status).to eq(201)
			expect(is_shortcode_format_valid(JSON.parse(last_response.body)["shortcode"])).to eq(true)
			expect(JSON.parse(last_response.body)["shortcode"]).to eq(valid_shortcode)
			expect(last_response.headers['Content-Type']).to eq('application/json')
		end
	end

	context 'when receiving a POST request to "/shorten" with payload containing "url" and previously registered "shortcode"' do
		it 'returns [409]' do
			post '/shorten', { url: example_url_1, shortcode: valid_shortcode }.to_json, { "CONTENT_TYPE" => "application/json" }
			expect(last_response.status).to eq(409)
			expect(last_response.headers['Content-Type']).to eq('application/json')
		end
	end

	context 'when receiving a POST request to "/shorten" with payload containing "url" and invalid "shortcode"' do
		it 'returns [422]' do
			post '/shorten', { url: example_url_1, shortcode: invalid_shortcode }.to_json, { "CONTENT_TYPE" => "application/json" }
			expect(last_response.status).to eq(422)
			expect(is_shortcode_format_valid(invalid_shortcode)).to eq(false)
			expect(last_response.headers['Content-Type']).to eq('application/json')
		end
	end

	context 'when receiving a GET request to "/:shorcode" with unknown shortcode' do
		it 'returns [404]' do
			get '/%s' % [unknown_shortcode]
			expect(last_response.status).to eq(404)
			expect(last_response.headers['Content-Type']).to eq('application/json')
		end
	end

	context 'when receiving a GET request to "/:shorcode" with known shortcode' do
		it 'returns [302] with "Location" header pointing to the registerd url' do
			get '/%s' % [valid_shortcode]
			expect(last_response.status).to eq(302)
			expect(last_response.headers).to have_key('Location')
			expect(last_response.headers['Location']).to eq(example_url_1)
		end
	end
 
	context 'when receiving a GET request to "/:shorcode/stats" with unknown shortcode' do
		it 'returns [404]' do
			get '/%s/stats' % [unknown_shortcode]
			expect(last_response.status).to eq(404)
			expect(last_response.headers['Content-Type']).to eq('application/json')
		end
	end

	context 'when receiving a GET request to "/:shorcode/stats" with known, previously accessed shortcode' do
		it 'returns [200] with "startDate", "lastSeenDate" and "redirectCount"' do
			get '/%s/stats' % [known_shortcode]
			expect(last_response.status).to eq(200)
			expect(JSON.parse(last_response.body)).to have_key('startDate')
			expect(JSON.parse(last_response.body)).to have_key('lastSeenDate')
			expect(JSON.parse(last_response.body)).to have_key('redirectCount')
			expect(JSON.parse(last_response.body)['redirectCount']).to be > 0
			expect(last_response.headers['Content-Type']).to eq('application/json')
		end
	end

	context 'when receiving a GET request to "/:shorcode/stats" with known, never accessed shortcode' do
		it 'returns [200] with "startDate" and "redirectCount"' do
			get '/%s/stats' % [new_shortcode]
			expect(last_response.status).to eq(200)
			expect(JSON.parse(last_response.body)).to have_key('startDate')
			expect(JSON.parse(last_response.body)).not_to have_key('lastSeenDate')
			expect(JSON.parse(last_response.body)).to have_key('redirectCount')
			expect(JSON.parse(last_response.body)['redirectCount']).to eq(0)
			expect(last_response.headers['Content-Type']).to eq('application/json')
		end
	end
end
