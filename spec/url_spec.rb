require 'url.rb'


describe '#is_url' do

	let(:valid_urls) {[
		'http://impraise.com',
		'http://www.impraise.com',
		'https://www.impraise.com',
		'https://www.impraise.com/section',
		'https://www.impraise.com/section?value=123',
		'https://www.impraise.com/section?value=123&argument=qwe',
		'https://www.impraise.com#title',
		'https://www.impraise.com/section?value=123&argument=qwe#title',
		'https://user:password@www.impraise.com/section?value=123&argument=qwe#title',
	]}
	let(:invalid_urls) {[
		'ftp://impraise.com',
		'smtp://impraise.com',
		'httpss://impraise.com',
		'http://impraise\section.com',
		'impraise.com',
	]}

	context 'when called with valid urls' do

		subject(:returned_values) { valid_urls.map {|url| is_url(url)} }

		it 'returns true' do
			expect(returned_values).to all(be true)
		end
	end

	context 'when called with invalid urls' do

		subject(:returned_values) { invalid_urls.map {|url| is_url(url)} }

		it 'returns false' do
			expect(returned_values).to all(be false)
		end
	end
end